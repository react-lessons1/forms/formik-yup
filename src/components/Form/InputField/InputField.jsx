import React from 'react';

export const InputField = ({
  error,
  touched,
  values,
  handleChange,
  type,
  name,
  placeholder,
}) => {
  return (
    <>
      <input
        type={type}
        name={name}
        placeholder={placeholder}
        onChange={handleChange}
        value={values}
      />
      {error && touched ? <div>{error}</div> : null}
    </>
  );
};
