import { useState } from 'react';
import styles from './FormLogin.module.scss';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { InputField } from './InputField/InputField';

const SignupSchema = Yup.object().shape({
  login: Yup.string()
    .min(3, 'too short')
    .max(10, 'too long')
    .matches(/^[0-9a-zA-Z_\-/.]+$/, 'wrong symbols')
    .required('required'),
  birthday: Yup.date().default(() => new Date()),
  email: Yup.string().email('invalid email').required('required'),
});

export function FormLogin() {
  const handleSubmit = (values) => {
    console.log(values);
  };

  const handleChange = (values) => {
    console.log('input');
  };

  return (
    <>
      <h1>SignUp</h1>
      <Formik
        initialValues={{
          login: '',
          email: '',
          birthday: '',
        }}
        validationSchema={SignupSchema}
        onSubmit={handleSubmit}
      >
        {({ errors, touched, handleChange, values }) => (
          <Form className={styles.FormBody}>
            <InputField
              name="login"
              type="text"
              placeholder="login"
              error={errors.login}
              touched={touched.login}
              handleChange={handleChange}
              values={values.login}
            />
            <InputField
              name="email"
              type="text"
              placeholder="email"
              error={errors.email}
              touched={touched.email}
              handleChange={handleChange}
              values={values.email}
            />
            <InputField
              name="birthday"
              type="date"
              placeholder=""
              error={errors.birthday}
              touched={touched.birthday}
              handleChange={handleChange}
              values={values.birthday}
            />
            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>
    </>
  );
}
