import { useState } from 'react';
import './App.css';

import { FormLogin } from '../Form/FromLogin';

export function App() {
  return (
    <div className="App">
      <FormLogin />
    </div>
  );
}
